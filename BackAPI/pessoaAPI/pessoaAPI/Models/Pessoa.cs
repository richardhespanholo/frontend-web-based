﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pessoaAPI.Models
{
    public class Pessoa
    {
        public long PessoaId { get; set; }
        public string Nome { get; set; }
        public DateTime dataNascimento { get; set; }
        public string cpf { get; set; }
        public string sexo { get; set; }
        public string endereco { get; set; }
    }
}