namespace pessoaAPI.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using pessoaAPI.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<pessoaAPI.Models.pessoaAPIContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(pessoaAPI.Models.pessoaAPIContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
          
            
        }
    }
}
