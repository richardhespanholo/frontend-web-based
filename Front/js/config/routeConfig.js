angular.module("pessoa").config(function ($routeProvider) {
    $routeProvider.when("/listar", {
        templateUrl: "view/lista.html",
        controller: "crudPessoaCtrl"
    });
     $routeProvider.when("/relatorio", {
        templateUrl: "view/relatorio.html",
        controller: "graficoCtrl"
     });
    $routeProvider.when("/voltar", { redirectTo: "/listar" });
    $routeProvider.when("/excluir/:id", {
        templateUrl: "view/excluir.html",
        controller: "detalhesPessoaCtrl"
    });
    $routeProvider.when("/cadastro", {
        templateUrl: "view/cadastro.html",
        controller: "crudPessoaCtrl"
    });
    $routeProvider.when("/editar/:id", {
        templateUrl: "view/editar.html",
        controller: "detalhesPessoaCtrl"
    });
    $routeProvider.otherwise({ redirectTo: "/listar" });
});