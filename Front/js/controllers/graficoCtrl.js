var app = angular.module("pessoa").controller("graficoCtrl", function ($scope, $http, $routeParams) {
    $scope.masculino = 0;
    $scope.feminino = 0;
    $scope.pessoas2 = [];
    $scope.pessoas = [];
    $scope.datateste = [];
    $scope.dataformatadaAno;
    $scope.dataformatadaMes;
    $scope.dataformatadaDia;
    $scope.de0a9 = 0;
    $scope.de10a19 = 0;
    $scope.de20a29 = 0;
    $scope.de30a39 = 0;
    $scope.de40mais = 0;
    var carregarGraficoMF = function () {
        $http.get("http://localhost:53137/api/Pessoas").then(function (response) {
            $scope.pessoas2 = response.data;
            $scope.pessoas = response.data;
            var x = 0;
            for (var i = 0; i < $scope.pessoas2.length; i++) {
                console.log($scope.pessoas2[i].sexo);
                if ($scope.pessoas2[i].sexo == "Masculino") {
                    $scope.masculino++;
                }
                else {
                    $scope.feminino++;
                }
            }
            var ctx = document.getElementById("myChart");
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ["Masculino", "Feminino"],
                    datasets: [{
                        label: 'Sexo',
                        data: [$scope.masculino, $scope.feminino],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
            while ($scope.pessoas[x] != null) {
                $scope.datateste[x] = $scope.pessoas[x].dataNascimento;
                $scope.dataformatadaAno = $scope.datateste[x].slice(0, 4);
                $scope.dataformatadaMes = $scope.datateste[x].slice(5, 7);
                $scope.dataformatadaDia = $scope.datateste[x].slice(8, 10);
                if (idade($scope.dataformatadaAno, $scope.dataformatadaMes, $scope.dataformatadaDia) < 10) {
                    $scope.de0a9++;
                }
                if (idade($scope.dataformatadaAno, $scope.dataformatadaMes, $scope.dataformatadaDia) >= 10 && idade($scope.dataformatadaAno, $scope.dataformatadaMes, $scope.dataformatadaDia) < 20) {
                    $scope.de10a19++;
                }
                if (idade($scope.dataformatadaAno, $scope.dataformatadaMes, $scope.dataformatadaDia) >= 20 && idade($scope.dataformatadaAno, $scope.dataformatadaMes, $scope.dataformatadaDia) < 30) {
                    $scope.de20a29++;
                }
                if (idade($scope.dataformatadaAno, $scope.dataformatadaMes, $scope.dataformatadaDia) >= 30 && idade($scope.dataformatadaAno, $scope.dataformatadaMes, $scope.dataformatadaDia) < 40) {
                    $scope.de30a39++;
                }
                if (idade($scope.dataformatadaAno, $scope.dataformatadaMes, $scope.dataformatadaDia) >= 40) {
                    $scope.de40mais++;
                }
                x++;
            }
            var ctx = document.getElementById("myChart2");
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ["De 0 a 9 anos", "De 10 a 19 anos", "De 20 a 29 anos", "De 30 a 39 anos", "Mais de 40 anos"],
                    datasets: [{
                        label: 'Faixa de Idade',
                        data: [$scope.de0a9, $scope.de10a19, $scope.de20a29, $scope.de30a39, $scope.de40mais],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    layout: {
                        padding: {
                            left: 50,
                            right: 0,
                            top: 0,
                            bottom: 0
                        }
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        });
    };
    carregarGraficoMF();
    function idade(ano_aniversario, mes_aniversario, dia_aniversario) {
        var d = new Date,
            ano_atual = d.getFullYear(),
            mes_atual = d.getMonth() + 1,
            dia_atual = d.getDate(),
            ano_aniversario = +ano_aniversario,
            mes_aniversario = +mes_aniversario,
            dia_aniversario = +dia_aniversario,
            quantos_anos = ano_atual - ano_aniversario;
        if (mes_atual < mes_aniversario || mes_atual == mes_aniversario && dia_atual < dia_aniversario) {
            quantos_anos--;
        }
        return quantos_anos < 0 ? 0 : quantos_anos;
    }
});