angular.module("pessoa").controller("detalhesPessoaCtrl", function ($scope, $routeParams, $http, $location) {
    $scope.pessoa = [];
    $scope.datateste;
    $scope.dataformatada;
    $scope.dataParaSalvar;
    $scope.enderecoFormatado;
    var carregarPessoa = function (id) {
        $http.get("http://localhost:53137/api/Pessoas/" + id).then(function (response) {
            $scope.pessoa = response.data;
            $scope.datateste = $scope.pessoa.dataNascimento;
            $scope.dataformatada = $scope.datateste.slice(0, 10);
            var teste = $scope.pessoa.endereco;
            $scope.enderecoFormatado = teste.split(",");
        });
    };
    carregarPessoa($routeParams.id);
    $scope.excluir = function (id) {
        $http.delete("http://localhost:53137/api/Pessoas/" + id).then(function (response) {
            $location.path("/voltar");
        });
    }
    $scope.modificar = function (id, pessoa) {
        pessoa.endereco = $scope.enderecoFormatado[0] + "," + $scope.enderecoFormatado[1] + "," + $scope.enderecoFormatado[2];
        if ($scope.dataParaSalvar) {
            pessoa.dataNascimento = $scope.dataParaSalvar;
        } else {
            console.log("errado");
        }
        $http.put("http://localhost:53137/api/Pessoas/" + id, pessoa).then(function (response) {
            $location.path("/voltar");
        });
    }
});