var app = angular.module("pessoa").controller("crudPessoaCtrl", function ($scope, $http, $location) {
    $scope.pessoas = [];
    $scope.endereco = { rua: "", bairro: "", numero: "" };
    var carregarPessoas = function () {
        $http.get("http://localhost:53137/api/Pessoas").then(function (response) {
            $scope.pessoas = response.data;
        });
    };
    carregarPessoas();
    $scope.cadastrar = function (pessoa) {
        var enderecoCerto = $scope.endereco.rua + "," + $scope.endereco.numero + "," + $scope.endereco.bairro;
        pessoa.endereco = enderecoCerto;
        $http.post("http://localhost:53137/api/Pessoas/", pessoa).then(function (response) {
            $scope.pessoas = response.data;
            $location.path("/voltar");
        });
    }
});